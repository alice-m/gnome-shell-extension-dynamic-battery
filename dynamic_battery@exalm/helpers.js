const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;
const GdkPixbuf = imports.gi.GdkPixbuf;

function _clutterColor2GdkRGBA(color) {
    return new Gdk.RGBA({
        red: color.red / 255,
        green: color.green / 255,
        blue: color.blue / 255,
        alpha: color.alpha / 255
    });
}

function setSourceIcon(cr, name, size, themeNode) {
    let gicon = Gtk.IconTheme.get_default().lookup_icon(name, size, 0);
    let colors = themeNode.get_icon_colors();
    let fColor = _clutterColor2GdkRGBA(colors.foreground);
    let sColor = _clutterColor2GdkRGBA(colors.success);
    let wColor = _clutterColor2GdkRGBA(colors.warning);
    let eColor = _clutterColor2GdkRGBA(colors.error);
    let pixbuf = gicon.load_symbolic(fColor, sColor, wColor, eColor)[0];
    Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
}

function drawIcon(cr, name, size, themeNode) {
    setSourceIcon(cr, name, size, themeNode);
    cr.rectangle(0, 0, size, size);
    cr.fill();
}

// This is a horrible hack, but it allows me to use custom string properties in CSS
function getString(themeNode, prop) {
    let url = themeNode.get_url(prop);
    if (typeof url == "string")
        return Gio.File.new_for_path(url).get_basename();
    else
        return url.get_basename();
}

function getBoolean(themeNode, prop) {
    return getString(themeNode, prop) == "true";
}

function setSourceIconFromCSS(cr, prop, size, themeNode, theme) {
    setSourceIcon(cr, theme + "-" + getString(themeNode, prop), size, themeNode);
}

function drawIconFromCSS(cr, prop, size, themeNode, theme) {
    drawIcon(cr, theme + "-" + getString(themeNode, prop), size, themeNode);
}

