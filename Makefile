NAME=dynamic_battery@exalm
INSTALL_PATH=~/.local/share/gnome-shell/extensions
SCHEMA=$(NAME)/schemas/gschemas.compiled

$(NAME).zip: $(SCHEMA) $(NAME)
	rm -rf $(NAME).zip
	cd $(NAME) && \
	zip ../$(NAME).zip * -r

$(SCHEMA):
	glib-compile-schemas $(NAME)/schemas

install: $(SCHEMA)
	mkdir -p $(INSTALL_PATH)
	rm -rf $(INSTALL_PATH)/$(NAME)
	cp $(NAME) $(INSTALL_PATH) -r

clean:
	rm -f $(SCHEMA)
	rm -f *.zip

.PHONY: install clean